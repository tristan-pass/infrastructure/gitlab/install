#!/usr/bin/env bash
echo "检查docker是否安装"
docker version

echo "检查磁盘是否够用"
df -h /data

echo "创建数据挂载点"
rm -rf /data/tristan/gitlab/config /data/tristan/gitlab/logs /data/tristan/gitlab/data
mkdir -p  /data/tristan/gitlab/config /data/tristan/gitlab/logs /data/tristan/gitlab/data

echo "拉取gitlab镜像"
docker pull gitlab/gitlab-ce:latest

echo "运行镜像"
docker run --detach \
  --hostname gitlab.yibainetworklocal.com \
  --publish 80:80 --publish 2289:22 \
  --name gitlab \
  --restart always \
  --volume /data/tristan/gitlab/config:/etc/gitlab \
  --volume /data/tristan/gitlab/logs:/var/log/gitlab \
  --volume /data/tristan/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest

echo "查看镜像运行情况"
docker logs -f gitlab
