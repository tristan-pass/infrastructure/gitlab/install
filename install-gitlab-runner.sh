#!/usr/bin/env bash
echo "拉取镜像"
docker pull gitlab/gitlab-runner:latest

echo "清理原来目录"
rm -rf /data/tristan/gitlab-runner/config

echo "创建挂载目录"
mkdir -p  /data/tristan/gitlab-runner/config

echo "检查磁盘可用"
df -h /data/tristan/gitlab-runner/config

echo "清理原来的gitlab-runner"
docker stop gitlab-runner
docker rm   gitlab-runner

echo "运行"
docker run -d --name gitlab-runner --restart always \
   -v /data/tristan/gitlab-runner/config:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   -e TZ=Asia/Shanghai \
   gitlab/gitlab-runner:latest

echo "查看日志: docker logs -f gitlab-runner"
sleep 1
docker logs gitlab-runner

echo "注册到gitlab: docker exec -it gitlab-runner gitlab-ci-multi-runner register"

echo "设置默认配置 /data/tristan/gitlab-runner/config/config.toml"
# 修改并发数
echo 'concurrent = 100'
# 修改磁盘挂载
echo '    volumes = ["/cache", "/root/.m2:/root/.m2","/var/run/docker.sock:/var/run/docker.sock","/etc/localtime:/etc/localtime:ro"]'
# 修改拉取镜像策略
echo '    pull_policy = "if-not-present"'
